# Analysis Centre Software - POD

## POD

The `ACS` Version 1.1.0 beta release supports:

1. The `POD` 

## Configuration Files

The `POD` Precise Orbit Determination (`./bin/pod`) uses the configuration files:

├── [POD.in](https://bitbucket.org/geoscienceaustralia/pod/src/thomas3/docs/POD.md) (Master POD configuration file

├── [EQM.in](https://bitbucket.org/geoscienceaustralia/pod/src/thomas3/docs/EQM.md) (Full force model equation of motion configuration)

├── [VEQ.in](https://bitbucket.org/geoscienceaustralia/pod/src/thomas3/docs/VEQ.md) (variational equations configuration)
    

    

